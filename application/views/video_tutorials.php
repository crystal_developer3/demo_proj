<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">TUTORIALS</h1>
              <p><a href="<?=base_url()?>">Home </a> / TUTORIALS</p>
            </div>
            <!-- Tutorial Strat Here -->
            <!-- <div class="tutorial">
              <div class="container padding_all">
                <div class="row">
                  <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <div class="tutorial-link">
                      <div class="title" style="margin-bottom: 20px;">
                        <h2><b><u>Video Series</u></b></h2>
                      </div>
                      <a href="<?=base_url('tutorials/video_tutorial_links/1')?>"><i class="fa fa-folder fa-2x"></i>  Plesk Onyx 2017 End User Tutorials <span>15</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->

            <div class="tutorial">
              <div class="container padding_all">
                <div class="row">
                    <div class="tutorial-link">
                      <div class="title" style="margin-bottom: 20px;">
                        <h2><b><u>Plesk End User Tutorial Videos</u></b></h2>
                      </div>
                      <?php 
                        if(isset($tutorial_details) && $tutorial_details !=null){ 
                          // echo "<pre>";print_r($tutorial_details);exit;
                          foreach ($tutorial_details as $key => $value) { ?>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 " style="margin-bottom: 25px">
                              <a href="<?=base_url('tutorials/view_video_tutorial/'.$value['seo_slug'])?>">
                                <i class="fa fa-file"></i>
                                <?=$value['tutorial_name']?>
                              </a>
                            </div>
                            <?php
                          }
                        }
                      ?>
                    </div>
                </div>
                <a href="<?=base_url()?>" class="btn btn-primary">Back</a>
              </div>
            </div>
            <!-- Tutorial End Here -->
            
            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 