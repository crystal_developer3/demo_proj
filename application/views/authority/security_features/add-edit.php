<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Securiy Features Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Securiy Features Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/security_features/view"; ?>">Securiy Features</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $security_feature_details !=null ? 'Edit' : 'Add'?> Securiy Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $security_feature_details !=null ? base_url('authority/security_features/update_security_feature') : base_url('authority/security_features/insert_security_feature'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="feature_id" value="<?= ($security_feature_details !=null) ? $security_feature_details[0]['id'] : ""; ?>">
                                            <label for="title">Feature Title :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'title[]',
                                                    'placeholder' => 'Feature Title',
                                                    'class' => 'form-control title txtonly',
                                                    'id' => 'title',
                                                    'value' => (isset($security_feature_details) && $security_feature_details !=null ? $security_feature_details[0]['title'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("title", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_title" style="color: #fc3a3a;"></span>
                                            <hr>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if ($security_feature_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                            <br><br>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/feature'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        var feature_id = $("#feature_id").val();
        $("#form").validate({
            rules: {
                'title[]': {required: true},
                /*'description[]': {required: true},*/
            },
            messages: {
                'title[]': "Please enter feature title",
                /*'description[]': "Please enter description",*/
            }
        });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if (check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            } else {
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            } else {
                return false;
            }
        } 
    </script>
<?php $this->view('authority/common/footer'); ?>