<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Category Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Category Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/post-management"; ?>">Post</a></li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">View Posting</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $post_details !=null ? base_url('authority/post-management/update_post') : base_url('authority/post-management/add_post'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <?php
                                $input_fields = array(
                                    'type' => 'hidden',
                                    'name' => 'post_id',
                                    'id' => 'post_id',
                                    'value' => (isset($post_details) && $post_details !=null ? $post_details[0]['post_id'] : ""),
                                ); 
                                echo form_input($input_fields);
                            ?>
                            <!-- ajax call to get subcat. list -->
                            <input type="hidden" id="sub_cat_id" value="<?= ($post_details !=null) ? $post_details[0]['sub_category_id'] : '';?>">
                            <!-- end -->

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="category_name">Category name :<span class="required">*</span></label>
                                    <?php 
                                        if ($category_details !=null) {
                                            $option[null] = 'Select'; 
                                            $data = array(
                                                'class' => 'form-control category_id',
                                                'id' => 'category_id',
                                                'disabled' => 'disabled',
                                            );
                                            foreach ($category_details as $key => $value) {
                                                $option[$value['category_id']] = $value['category_name'];
                                            }
                                        }
                                        echo form_dropdown('category_id',$option,isset($post_details[0]['category_id']) ? $post_details[0]['category_id'] : '',$data);
                                    ?>
                                    <span class="error_cat_name" style="color: #fc3a3a;"></span>
                                    <?php echo form_error("category_name", "<div class='error'>", "</div>"); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><br>
                                    <label for="category_name">Sub category name :</label>

                                    <select class="form-control sub_category_id" name="sub_category_id" id="sub_category_id" disabled="">
                                        <option value="">Select sub category</option>
                                    </select>

                                    <?php echo form_error("sub_category_name", "<div class='error'>", "</div>"); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><br>                                    
                                    <label for="add_type">Add Type :<span class="required">*</span></label> 
                                    
                                    <label class="radio-inline"><input type="radio" name="add_type" value="Individual" <?= $post_details[0]['add_type'] == 'Individual' ? 'checked' : '';?> disabled>Individual
                                    </label>
                                    <label class="radio-inline"><input type="radio" name="add_type" value="Business" <?= $post_details[0]['add_type'] == 'Business' ? 'checked' : '';?> disabled>Business
                                    </label>
                                    <?php
                                        echo form_error("add_type", "<div class='error'>", "</div>");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><br>
                                    <label for="i_want">I Want :<span class="required">*</span></label> 
                                    <?php
                                        $input_fields = array(
                                            'name' => 'i_want',
                                            'placeholder' => 'I Want',
                                            'class' => 'form-control',
                                            'id' => 'i_want',
                                            'disabled' => 'disabled',
                                            'value' => (isset($post_details) && $post_details !=null ? $post_details[0]['i_want'] : ""),
                                        );
                                        echo form_input($input_fields);
                                        echo form_error("i_want", "<div class='error'>", "</div>");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><br>
                                    <label for="price">Price :<span class="required">*</span></label> 
                                    <?php
                                        $input_fields = array(
                                            'name' => 'price',
                                            'placeholder' => 'I Want',
                                            'class' => 'form-control only_digits',
                                            'id' => 'price',
                                            'disabled' => 'disabled',
                                            'value' => (isset($post_details) && $post_details !=null ? $post_details[0]['price'] : ""),
                                        );
                                        echo form_input($input_fields);
                                        echo form_error("price", "<div class='error'>", "</div>");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><br>
                                    <label for="post_description">Post description :<span class="required">*</span></label> 
                                    <?php
                                        $input_fields = array(
                                            'name' => 'post_description',
                                            'placeholder' => 'Post name',
                                            'class' => 'form-control post_description',
                                            'id' => 'editor1',
                                            'disabled' => 'disabled',
                                            'value' => (isset($post_details) && $post_details !=null ? $post_details[0]['post_description'] : ""),
                                        );
                                        echo form_textarea($input_fields);
                                        echo form_error("post_description", "<div class='error'>", "</div>");
                                    ?>
                                    <label class="error_description"></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php
                                    if ($similar_img_details != null) {
                                    ?>
                                        <div class="form-group">
                                            <label for="last_name">Post image:</label><br>
                                            <?php 
                                                foreach ($similar_img_details as $key => $value){
                                                    $img_id = $value['image_id'];
                                                ?>
                                                    <img src="<?= base_url(POST_NEED_IMG.'thumbnail/').$value['similar_image']?>" height="50px" width="50px" style="margin: 0 10px 0px 0px;">
                                                <?php }
                                            ?>                                                
                                        </div>
                                    <?php }
                                ?> 
                            </div>
                            <!-- <div class="col-md-12" style="margin-top: 2%;">
                                <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                <a href="<?php echo site_url() . 'authority/post-management'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>

    <script>
        /*FORM VALIDATION*/
        var post_id = $("#post_id").val();
        $("#form").validate({
            rules: {
                category_id: {required: true},       
                add_type: {required: true},       
                i_want: {required: true},       
                // post_description: {required: true},       
                price: {required: true},       
                // post_image: { 
                //     required: function(element) {
                //         if (post_id == '') {  
                //             return true;
                //         }
                //         else {
                //             return false;
                //         }
                //     }, 
                // },                  
            },
            messages: {
                category_id: "Please select category name",
                add_type: "Please select add type",
                i_want: "Please Enter I-want",
                // post_description: "Please Enter description",
                price: "Please Enter price",
                // post_image: "Please select image",       
            }
        });
        $("form").submit(function () {
            var nicInstance = nicEditors.findEditor('editor1');
            var dis = nicInstance.getContent();

            if ($("#form").valid()){
                if (dis =="<br>"){
                    $('.error_description').text('Please enter description').css({'color':'#e81123','font-size':'11px'});;
                    return false;
                }
            }
        });

        $(document).ready(function() {
            get_sub_category();
        });

        function get_sub_category(){
            var cat_id = $('#category_id').val();
            var sub_cat_id = $('#sub_cat_id').val();
            $.ajax({
                url: "<?= base_url('authority/sub_of_subcat/get_sub_cat_name')?>",
                data: {category_id:cat_id,sub_cat_id:sub_cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    // alert(data);
                    $("#sub_category_id").html(data); 
                }
            });
        }

        $("#category_id").change(function(){
            var cat_id = $('#category_id').val();
            // alert(cat_id);
            $.ajax({
                url: "<?= base_url('authority/sub_of_subcat/get_sub_cat_name')?>",
                data: {category_id:cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    // alert(data);
                    $("#sub_category_id").html(data); 
                }
            });
        });

        // $('.check').click(function(){
        //     var post_id = $("#post_id").val();

        //     var cat_name = $(".post_title_english").val();
        //     var cat_image = $(".cat_image")[0].files.length;

        //     if (cat_name_english =='' && cat_name_arabic ==''){
        //         $('.error_cat_name').text('Please enter english OR arabic category-name.');
        //         $('.post_title_arabic').focus();
        //         return false;
        //     }  

        //     if (post_id == ''){
        //         if(cat_image === 0){
        //             $('.error_file').text("Please select image.");
        //             return false;
        //         }
        //     } 
        // });

        // $('.check_excel').click(function(){
        //     if(isemptyfocus('xls_file'))
        //     {
        //         return false;
        //     }
        // });
    </script>
<?php $this->view('authority/common/footer'); ?>