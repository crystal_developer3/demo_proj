<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Feedback</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Feedback</li>
		</ol>
		<?php $this->load->view('authority/common/messages');?>
        <?php
			if (isset($success)) {
			?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $success; ?>
			</div>
            <?php
			}
		?>
	</section>
    <section class="content">
        <!-- /.row -->
		<!-- <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">SEARCH</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
                    <?php
						if (isset($user_details) && $user_details !=null) {
							?>
                    <div class="box-body">
                        <div class="filter-section">
                           <form action="<?= base_url('authority/feedback/view')?>" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
                                            <label for="filter_by_day">Start Date:</label>
                                            <input class="form-control filter_date" id="start_date" name="start_date" placeholder="Start Date" type="text" value="<?php if($start_date==""){}else{ echo $start_date;} ?>">
										</div>
									</div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="filter_by_day">End Date:</label>
                                            <input class="form-control filter_date" id="end_date" name="end_date" placeholder="End Date" type="text" value="<?php if($end_date==""){}else{ echo $end_date;} ?>">
										</div>
									</div>									
									<div class="col-md-2">
										<div class="form-group">
											<label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
											<button type="submit" class="btn btn-default form-control check btn-sm" name="submit" value="submit">SEARCH</button>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
											<button type="submit" class="btn btn-primary form-control btn-sm" formaction="<?= base_url('authority/feedback/excel_genrate_report')?>">Export Excel</button>
										</div>
									</div>
								</div>
							</form> 
						</div>
					</div> 
					<?php } ?>                   
				</div>
			</div>
		</div> -->
		<div class="clearfix"></div>
        <div class="row">
        	<form method="post">
	            <div class="col-xs-12">
	            	<?php
						if (isset($user_details) && $user_details !=null) {
							?>
								<input type="submit" class="btn btn-sm btn-danger chk_submit" value="Delete all" formaction="<?= base_url('authority/feedback/multiple_delete')?>">
							<?php
						}
					?>
	                <div class="box">
	                    <?php /*
							<div class="box-header">
							<div class="box-tools">
							<div class="input-group input-group-sm">
							<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
							<div class="input-group-btn">
							<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
							</div>
							</div>
							</div>
						* * */ ?>
	                    <!-- /.box-header -->
	                    <div class="box-body table-responsive no-padding">
	                        <table id="mytable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                	<th>
											<div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
										</th>
	                                    <th>No</th>
	                                    <th>Name</th>
										<th>Email</th>
	                                    <th>Message</th>
										<th data-hide="phone,medium">Status<br/><small>(Click to change status)</small></th>
	                                    <th>Create Date</th>
	                                    <th data-hide="phone,medium"  align="center">Action</th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if (isset($user_details) && $user_details !=null):
		                                    foreach ($user_details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
												<tr data-expanded="true">
													<td>
														<div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
													</td>
													<td><?= $no+$key;?></td>
													<!-- <td>
														<img src="<?= base_url(PROFILE_PICTURE).$value['profile_picture']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
													</td> -->
													<td><?= $value['name'];?></td>
													<td><?= $value['email'];?></td>
													<td><?= substr($value['message'], 0,100);?></td>
													<td>
														<?php 
															if($value['status'] == '1'){
																echo '<span class="label label-success change-status" data-table="feedback" data-id="'.$id.'" data-current-status="1">Active</span>';
																} else {
																echo '<span class="label label-danger change-status" data-table="feedback" data-id="'.$id.'" data-current-status="0">Deactive</span>';
															} 
														?>
													</td>
													<td><?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?></td>
													<td class="action" align="center">
														<p data-placement="top" data-toggle="tooltip" title="Details"><a href="<?php echo site_url(); ?>authority/feedback/details/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></p>
														<!-- <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/feedback/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p> -->
														<!-- <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/feedback/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p> -->
														<p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/feedback/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if (isset($pagination)) 
	                                    { 
	                                        echo $pagination;
	                                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>
						<?php /* if (isset($links) && $links != "") { ?>
							<div class="box-footer clearfix">
								<?php echo $links; ?>
							</div>
							<?php
							} */
						?>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</form>
		</div>
	</section>
</div>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<!-- DELETE POPUP -->
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {	
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			// alert(current_status);
			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_common_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						window.location = window.location.href;
					}
				}
			});
		});
		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});
	$('.check').click(function(){
        if(isemptyfocus('start_date') || isemptyfocus('end_date'))
        {
            return false;
        }
    });
    // multiple delete //
    $('.chk_submit').on('click', function() {
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		confirm('Are you sure you want to delete this item?');
	        	return true;
            }
        }
	});
</script>
<?php $this->view('authority/common/footer'); ?>									