<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">

<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Product</h1>        
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Product</li>
		</ol>
        <?php $this->view('authority/common/messages'); ?>
	</section>
	
	<section class="content-header">
        <div class="form-group">
            <div class="input-group">
                 <span class="input-group-addon">Search</span>
                 <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section>

    <section class="content">
        <!-- /.row -->
		<div class="clearfix"></div>
        <div class="row">
        	<form method="post">
	            <div class="col-xs-12">
	            	<?php
						if ($data !=null) {
							?>
								<input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/product_manage/multiple_delete')?>">
							<?php
						}
					?>
	                <a href="<?= base_url('authority/product_manage/add')?>" class="btn btn-md btn-primary">Add</a> 
	                <div class="box">	                    
	                    <div class="box-body table-responsive no-padding">
	                        <table id="myTable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                	<th>
											<div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
										</th>
	                                    <th>No</th>
	                                    <th style="min-width: 140px;">Category name</th>
                                        <th style="min-width: 150px;">Sub category name</th>
	                                    <th style="min-width: 150px;">Product name</th>
	                                    <th style="min-width: 250px;">Add modified date</th>
	                                    <th>Image</th>
										<th style="min-width: 150px;" data-hide="phone,medium">Status<br/><small>(Click to change status)</small></th>
	                                    <!-- <th style="min-width: 150px;">New arrivals</th> -->
	                                    <th style="min-width: 150px;">Latest product</th>
	                                    <th style="min-width: 150px;">Related product</th>
	                                    <th style="min-width: 150px;">Best seller</th>
	                                    <th style="min-width: 150px;">Populer product</th>
	                                    <th data-hide="phone,medium" align="center">Action</th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if ($data !=null):
											// echo"<pre>"; print_r($data); exit;
		                                    foreach ($data  as $key => $value) {
		                                    	$id = $value['product_id'];
											?>
												<tr data-expanded="true">
													<td>
														<div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
													</td>
													<td><?= $no+$key;?></td>

													<td>
														<?php
	                                                        $tmp = $this->Production_model->get_all_with_where('category','','',array('category_id'=>$value['category_id']));    
	                                                        echo $tmp !=null ? $tmp[0]['category_name'] : ''; 
	                                                        //print_r($category_details);exit;
	                                                    ?>
													</td>
													<td>
														<?php
	                                                        $tmp = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$value['sub_category_id']));
	                                                        echo $tmp !=null ? $tmp[0]['sub_category_name'] : ''; 
	                                                    ?>
													</td>													
													<td><?= $value['product_name']?></td>
													<td>
														<b>Added Date : </b> <?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?><br>
														<b>Modified Date : </b> <?= ($value['modified_date'] != '0000-00-00 00:00:00') ? date('d-m-Y h:i:s A',strtotime($value['modified_date'])) : '';?>
													</td>

													<td>
														<img src="<?= base_url(PRODUCT_IMAGE.'thumbnail/thumb/').$value['product_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
													</td>

													<td align="center">
														<?php 
															if($value['status'] == '1'){
																echo '<span class="label label-success change-status" data-table="product_manage" data-id="'.$id.'" data-current-status="1" data-parent_id="'.$value['category_id'].'">Active</span>';
																} else {
																echo '<span class="label label-danger change-status" data-table="product_manage" data-id="'.$id.'" data-current-status="0" data-parent_id="'.$value['sub_category_id'].'">Deactive</span>';
															} 
														?>
													</td>

													<!-- <td align="center">
														<?php 
															if($value['new_arrivals_product'] == 1){
	                                                            echo '<span class="label label-success change-new-arriavl-status" data-table="product_manage" data-id="'.$id.'" data-current-status="1">Active</An>';
	                                                            } else {
	                                                            echo '<span class="label label-danger change-new-arriavl-status" data-table="product_manage" data-id="'.$id.'" data-current-status="0">Deactive</spDean>';
	                                                        } 
														?>
													</td> -->
													<td align="center">
														<?php 
															if($value['latest_product'] == 1){
	                                                            echo '<span class="label label-success change-latest-status" data-table="product_manage" data-id="'.$id.'" data-current-status="1">Active</span>';
	                                                            } else {
	                                                            echo '<span class="label label-danger change-latest-status" data-table="product_manage" data-id="'.$id.'" data-current-status="0">Deactive</span>';
	                                                        }
														?>
													</td>
													<td align="center">
														<?php 
															if($value['related_product'] == 1){
	                                                            echo '<span class="label label-success change-related-status" data-table="product_manage" data-id="'.$id.'" data-current-status="1">Active</span>';
	                                                            } else {
	                                                            echo '<span class="label label-danger change-related-status" data-table="product_manage" data-id="'.$id.'" data-current-status="0">Deactive</span>';
	                                                        } 
														?>
													</td>

													<td align="center">
														<?php 
															if($value['best_seller'] == 1){
	                                                            echo '<span class="label label-success change-best_seller" data-table="product_manage" data-id="'.$id.'" data-current-status="1">Active</span>';
	                                                            } else {
	                                                            echo '<span class="label label-danger change-best_seller" data-table="product_manage" data-id="'.$id.'" data-current-status="0">Deactive</span>';
	                                                        } 
														?>
													</td>

													<td align="center">
														<?php 
															if($value['populer_product'] == 1){
	                                                            echo '<span class="label label-success change-populer_product" data-table="product_manage" data-id="'.$id.'" data-current-status="1">Active</span>';
	                                                            } else {
	                                                            echo '<span class="label label-danger change-populer_product" data-table="product_manage" data-id="'.$id.'" data-current-status="0">Deactive</span>';
	                                                        } 
														?>
													</td>

													<td class="action" align="center">
														<p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/product_manage/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
														
														<p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/product_manage/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if (isset($links)) 
	                                    { 
	                                        echo $links;
	                                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>

						<?php /* if (isset($links) && $links != "") { ?>
							<div class="box-footer clearfix">
								<?php echo $links; ?>
							</div>
							<?php
							} */
						?>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</form>
		</div>
	</section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
				
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {	
		
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_product_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.error) {
                        alert('Please active parent category...!');
                    }
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on('click','.change-new-arriavl-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change-new-arriavl-status',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_new_arriavl_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on('click','.change-latest-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change-special-status',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_latest_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on('click','.change-related-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change-related-status',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_related_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on('click','.change-best_seller',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change-best_seller',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_best_seller',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on('click','.change-populer_product',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var parent_id = jQuery(this).data('parent_id');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_populer_product',
				'id': id,
				'table': table,
				'current_status': current_status,
				'parent_id': parent_id,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_populer_product',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						//window.location = window.location.href;
					}
				}
			});
		});

		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});

    // multiple delete //
    $('.chk_submit').on('click', function() {
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		confirm('Are you sure you want to delete this item?');
	        	return true;
            }
        }
	});
</script>
<?php $this->view('authority/common/footer'); ?>									