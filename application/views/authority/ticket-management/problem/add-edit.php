<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<style type="text/css">
    .footer-wrap-layout1{
        margin-top: 16%;
    }
</style>

<div class="content-wrapper">
    <section class="content">
        <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3><?= $form_title?></h3>
                <ul>
                    <li><a href="<?= base_url('authority/dashboard')?>">Home</a></li>
                    <li><?= $form_title?></li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->
            <!-- Admit Form Area Start Here -->
        
            <div class="card height-auto">

                <?php $action = ($problem_details == null) ? base_url('authority/ticket_management/insert_problem') : base_url('authority/ticket_management/update_problem')?>
                <form class="new-added-form" id="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">       
                    <input type="hidden" name="id" value="<?= isset($problem_details) && $problem_details !=null ? $problem_details[0]['id'] : "";?>">
                    <div class="card-body">                   
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label>Ticket Problem *</label>
                                <input type="text" name="problem_name" id="problem_name" placeholder="Ticket Problem" class="form-control" value="<?= isset($problem_details) && $problem_details !=null ? $problem_details[0]['problem_name'] : "";?>">
                                <?php echo form_error("problem_name", "<div class='error'>", "</div>"); ?>
                            </div>                  
                            <div class="col-xl-2 col-lg-6 col-12 form-group">
                                <label>&nbsp;</label>
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            </div>                    
                            <div class="col-xl-2 col-lg-6 col-12 form-group">
                                <label>&nbsp;</label>
                                <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            problem_name: {required:true},            
        },
        messages: {
            problem_name: "Please enter location name",
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>