<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url() . "authority/users/view"; ?>"> Users</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?php
                $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data");
                echo form_open("", $attributes);
                ?>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $form_title; ?></h3>
                        <div class="box-tools pull-right">
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                        ?>
                        
                        <div class="form-group">
                            <label for="full_name">First name:<span class="required">*</span></label>
							<?php 
							$input_field = array(
							'type' => 'text',
							'name' => 'name',
							'id' => 'name',
							'class' => 'form-control',
							'placeholder' => 'Enter name',
							'value' => isset($name) ? $name : '',
							);
							echo form_input($input_field);
							echo form_error("name", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="mobile_no">Mobile no:<span class="required">*</span></label>
							<?php 
							$input_field = array(
								'type' => 'text',
								'name' => 'mobile_no',
								'id' => 'mobile_no',
								'class' => 'form-control only_digits',
								'placeholder' => 'Enter Mobile no',
                                'maxlength' => '10',
								'value' => isset($mobile_no) ? $mobile_no : '',
							);
							echo form_input($input_field);
                            echo form_error("mobile_no", "<div class='error'>", "</div>"); ?>
                        </div>                        
						<div class="form-group">
                            <label for="last_name">Email:<span class="required">*</span></label>
							<?php 
							$input_field = array(
								'type' => 'text',
								'name' => 'user_email',
								'id' => 'user_email',
								'class' => 'form-control',
                                'placeholder' => 'Enter email address',
								'value' => isset($user_email) ? $user_email : '',
							);
							echo form_input($input_field);
                            echo form_error("user_email", "<div class='error'>", "</div>"); 
							if(isset($user_email_err) && $user_email_err != ''){
								echo '<div class="error">'.$user_email_err.'</div>';
							}
							?>							
                        </div>
                        <div class="form-group">
                            <label for="last_name">Address:<span class="required">*</span></label>
                            <?php 
                            $input_field = array(
                                'type' => 'text',
                                'name' => 'address',
                                'id' => 'address',
                                'class' => 'form-control',
                                'placeholder' => 'Enter address',
                                'value' => isset($address) ? $address : '',
                            );
                            echo form_input($input_field);
                            echo form_error("address", "<div class='error'>", "</div>"); 
                            if(isset($address_err) && $address_err != ''){
                                echo '<div class="error">'.$address_err.'</div>';
                            }
                            ?>                          
                        </div>
                        <div class="form-group">
                            <label for="country">Country:<span class="required">*</span></label>
                            <?php
                                $country_info = get_country();
                                $options = array();
                                $options[NULL] = 'Select Country';
                                if (count($country_info) > 0) {
                                    foreach ($country_info as $key => $value) {
                                        $options[$value['id']] = $value['country_name'];
                                    }
                                }
                                echo form_dropdown('id_country', $options, isset($id_country) ? $id_country : '', 'id="id_country" class="form-control"');
                                echo form_error('id_country', '<label class="error">', '</label>');
                            ?>
                        </div> 

                        <div class="form-group">
                            <label for="country">State:<span class="required">*</span></label>
                            <?php
                                if (isset($id_country) && $id_country != '') {
                                    $state_info = get_state($id_country);
                                    // echo"<pre>"; echo $this->db->last_query(); print_r($state_info);
                                    $options = array();
                                    $options[NULL] = 'Select State';
                                    if (count($state_info) > 0) {
                                        foreach ($state_info as $key => $value) {
                                            $options[$value['id']] = $value['state_name'];
                                        }
                                    }
                                } else {
                                    $options = array(
                                        NULL => 'Select State',
                                    );
                                }
                                echo form_dropdown('id_state', $options, isset($id_state) ? $id_state : '', 'id="id_state" class="form-control"');
                                echo form_error('id_state', '<label class="error">', '</label>');
                            ?>
                        </div> 

                        <div class="form-group">
                            <label for="country">State:<span class="required">*</span></label>
                            <?php
                                if (isset($id_state) && $id_state != '') {
                                    $city_info = get_city($id_state);
                                    $options = array();
                                    $options[NULL] = 'Select City';
                                    if (count($city_info) > 0) {
                                        foreach ($city_info as $key => $value) {
                                            $options[$value['id']] = $value['city_name'];
                                        }
                                    }
                                } else {
                                    $options = array(
                                        NULL => 'Select City',
                                    );
                                }
                                echo form_dropdown('id_city', $options, isset($id_city) ? $id_city : '', 'class="form-control" id="id_city"');
                                echo form_error('id_city', '<label class="error">', '</label>');
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender:<span class="required">*</span></label>
                            <?php
                            $gender_info = array("Male", "Female");
                            ?>
                            <select class="form-control" name="gender" id="gender">
                                <?php
                                if (isset($gender_info) && !empty($gender_info)) {
                                    $selected = (isset($gender) ? $gender : "");
                                    foreach ($gender_info as $value) {
                                        $selected_text = "";
                                        if ($value == $selected) {
                                            $selected_text = "selected='selected'";
                                        }
                                        echo "<option value='" . $value . "' " . $selected_text . ">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <?php echo form_error("gender", "<div class='error'>", "</div>"); ?>
                        </div>    
                        <?php /*
						<div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" name="address" id="address"><?php echo (isset($address) ? $address: ""); ?></textarea>
                        </div>
						                    
                        <div class="form-group">
                            <label for="profile_photo">Profile Photo:</label>
                            <input type="file" name="profile_photo" id="profile_photo" />
                            <?php echo form_error("profile_photo", "<div class='error'>", "</div>"); ?>
                            <?php
                            if (isset($profile_photo_error) && $profile_photo_error != "") {
                                echo "<div class='error'>" . $profile_photo_error . "</div>";
                            }
                            ?>
                        </div> */ ?>
                        <div class="form-group">
                            <input class="btn btn-success text-uppercase" value="Submit" type="submit">
                            <a href="<?php echo site_url() . 'authority/userlist/view'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>                          
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>
<script>
	$(document).ready(function(){
		/*FORM VALIDATION*/
		$("#form").validate({
			rules: {
                user_type: {required:true},            
				name: {required:true},            
				mobile_no: {required:true},
				user_email: {required: true,email: true},
			},
			messages: {
                user_type: "Please select user type",
				name: "Please enter name",
				mobile_no: "Please enter mobile no",
				user_email: 'Please enter email address',
			}
		});
	})
</script>
<?php $this->view('authority/common/footer'); ?>