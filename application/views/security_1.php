<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/demo.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/dialog.css" />
        <!-- individual effect -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/dialog-sally.css" />
        <script src="<?= base_url()?>assets/popup_js/modernizr.custom.js"></script>
        <style type="text/css">
          @import url(<?= base_url()?>assets/css/mdb.css);
        </style>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">Security</h1>
              <p><a href="<?=base_url()?>">Home </a> / Security</p>
            </div>
            <!-- SSL Certificate Plan Strat Here -->
          <div class="ssl-cerificate-plans">
            <div class="container">
              <div class="row">
                <?php 
                  if (isset($security_data) && $security_data !=null){ 
                    $i = 1;
                    foreach ($security_data as $security_key => $s_value) {
                        ?>
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 aos-init aos-animate mobile-top" data-aos="flip-left">
                          <div class="hosting-package">
                            <div class="plan-header">
                              <h3 class="text-center"><?=$s_value['title']; ?></h3>
                            </div>
                            <div class="plan-content">
                              <div class="select-turner">
                                <form method="post" name="security_order" class="security_order">
                                    <div class="form-group">
                                      <label for="exampleFormControlSelect1">SELECT THE TENURE</label>
                                      <select class="form-control" class="exampleFormControlSelect1" style="background: none; border-color: #ffffff; color: #ffffff">
                                          <option style="color: #000;">1 Year @ Rs. 1,452/yr</option>
                                          <option style="color: #000">2 Year @ Rs. 1,452/yr</option>
                                      </select>
                                    </div>
                                </form>
                                <div class="btn2">
                                  <button type="button" class="btn_order trigger" data-dialog="somedialog-<?=$i?>">Order Now</button>
                                </div>
                              </div>
                              <hr>
                              <div id="somedialog-<?=$i?>" class="dialog">
                                <div class="dialog__overlay"></div>
                                <div class="dialog__content">
                                  <!-- <form method="post" name="search_domain" id="search_domain"> -->
                                    <div class="form-group">
                                      <label class="ord_class_label">Name</label>
                                      <input type="text" name="name" id="name" class="input_all ord_class_text">
                                    </div>
                                    <div class="form-group">
                                      <label class="ord_class_label">Email</label>
                                      <input type="email" name="email" id="email" class="input_all ord_class_text">
                                    </div>
                                    <div class="form-group">
                                      <label class="ord_class_label">Mobile Number</label>
                                      <input type="text" name="mobile_no" id="mobile_no" class="input_all ord_class_text">
                                    </div>
                                    <div class="form-group text-center">
                                      <button type="button" class="btn_order check">Submit</button>
                                    </div>
                                  <!-- </form> -->
                                  <hr>
                                  <div style="float: right;margin-right: 4%">
                                    <button class="btn_order action" data-dialog-close>Close</button>
                                  </div>
                                </div>
                              </div>
                              <div class="plan-list">
                                <ul>
                                  <?php
                                      if($s_value['id']!="") {
                                         $total = count($features_details);
                                        foreach ($features_details as $feature_key => $feature_value) { 
                                            $get_feature = $this->Production_model->get_all_with_where('security_related_feature','','',array('security_id'=>$s_value['id'],'feature_id'=>$feature_value['id'])); ?>
                                          <li >
                                            <?=(isset($get_feature) && $get_feature!=null)?'<span><i class="fa fa-check text-success" aria-hidden="true"></i></span> ':'<span><i class="fa fa-times" aria-hidden="true"></i></span> '?>
                                              <?=$feature_value['title']; ?></li>
                                          <?php
                                        }
                                      }
                                    ?>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php
                      $i++;
                    }
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- SSL Certificate Plan End Here -->
          <!-- Why SSl Certificates Area Strat Here -->
          <div class="why-ssl-certificates">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="main-title">                
                    <h3 class="text-center">Why SSL Certificates?</h3>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="why-certificates1 border">
                    <div class="icon">
                      <img src="<?=base_url('assets/image/icon/sensetive_info.png')?>" class="img-responsive center-block">
                    </div>
                    <div class="title">
                      <h4 class="text-center"><b>PROTECT SENSITIVE INFORMATION</b></h4>
                    </div>
                    <div class="content">
                      <p class="text-center">An SSL Certificate or HTTPS Certificate creates a secure environment to accept debit/credit payments and helps boost customer trust.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="why-certificates1 border">
                    <div class="icon">
                      <img src="<?=base_url('assets/image/icon/google_ranking.png')?>" class="img-responsive center-block">
                    </div>
                    <div class="title">
                      <h4 class="text-center"><b>BOOST GOOGLE RANKING</b></h4>
                    </div>
                    <div class="content">
                      <p class="text-center">Provide a safer browsing experience by adding an SSL Certificate for your website and enjoy the benefit of a better search engine ranking..</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="why-certificates1 border">
                    <div class="icon">
                      <img src="<?=base_url('assets/image/icon/secure_warning.png')?>" class="img-responsive center-block">
                    </div>
                    <div class="title">
                      <h4 class="text-center"><b>AVOID 'NOT SECURE' WARNING</b></h4>
                    </div>
                    <div class="content">
                      <p class="text-center">Protect your website with the green padlock by a trusted SSL service provider and avoid the not secure warning in the browser.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Why SSL Certificates Area End Here -->
          <!-- How It Work Area Strat Here -->
          <div class="how_it_work">
            <div class="container">
              <div class="main-title">
                <h3 class="text-center"> How It Work</h3>
              </div>
              <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top text-center aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="how_it_work1 text-center border1">
                    <div class="how_it_work_number text-center">
                      <h4>1</h4>
                    </div>
                    <div class="how_it_work_title">
                      <h4>ssl handeshake</h4>
                    </div>
                    <div class="how_it_work_content">
                      <p>When your customer visits your SSL protected site, the SSL Certificate creates an encrypted connection with their browser.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top text-center aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="how_it_work1 text-center border1">
                    <div class="how_it_work_number text-center">
                      <h4>2</h4>
                    </div>
                    <div class="how_it_work_title">
                      <h4>Padlock in address bar</h4>
                    </div>
                    <div class="how_it_work_content">
                      <p>On secure encryption, a padlock icon will appear in your customers web browser to notify them their connection is secure.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mobile-top text-center aos-init aos-animate" data-aos="zoom-in-up">
                  <div class="how_it_work1 text-center border1">
                    <div class="how_it_work_number text-center">
                      <h4>3</h4>
                    </div>
                    <div class="how_it_work_title">
                      <h4>your website is secure!</h4>
                    </div>
                    <div class="how_it_work_content">
                      <p>With Comodo certificates the information passed to and from your website are provided upto 128/256-bit encryption.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- How It Work Area End Here -->
          <!-- Visitor Area Strat Here -->
          <div class="visitor">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="main-title">                
                    <h3 class="text-center">How Visitors See Your Site With & Without SSL</h3>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mobile-top aos-init aos-animate" data-aos="flip-left">
                  <div class="without-ssl">
                    <img src="<?=base_url('assets/image/icon/without_ssl.png')?>" class="img-responsive">
                    <div class="witout-ssl-title">
                      <h5>Witout SSL</h5>
                    </div>
                    <div class="witout-ssl-content">
                      <ul>
                        <li>Connection is not encrypted</li>
                        <li>Browser Like Chrome  Mark All HTTP Pages As Not Secure</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mobile-top aos-init aos-animate" data-aos="flip-left">
                  <div class="without-ssl">
                    <img src="<?=base_url('assets/image/icon/with_ssl.png')?>" class="img-responsive">
                    <div class="witout-ssl-title">
                      <h5>With SSL</h5>
                    </div>
                    <div class="witout-ssl-content">
                      <ul>
                        <li>Secure Connection Between Browser And Server</li>
                        <li>Trusted Enviroment For Website Visitor</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Visitors Area End Here -->
          <!-- footer us start -->
            <!-- email start -->
            <!-- <div class="all_email">
              <div class="container padding_all">
                <div class="row margin_top">
                <?php 
                    if (isset($security_data) && $security_data !=null){ 
                      $i=1; 
                      foreach ($security_data as $key => $value1) { ?>
                        <div class="col-md-6 col-xs-12 <?php if($i%2==0) {?> email_color_one <?php }else{?>email_color_two<?php } ?>">
                          <div class="col-xs-1"><i class=" fa fa-check-square-o"></i></div>
                          <div class="col-xs-11">
                            <h5><?php echo $value1['name']; ?></h5>
                            <p><?php echo $value1['details']; ?></p>
                          </div>
                        </div>
                        <?php $i++; 
                      } 
                    } 
                  ?> 
                </div>
              </div>
            </div> -->
            <!-- email over -->
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/js/mdb.min.js"></script>
      <script src="<?= base_url()?>assets/popup_js/classie.js"></script>
      <script src="<?= base_url()?>assets/popup_js/dialogFx.js"></script>
      <script>
        (function() {

          var dlgtrigger = document.querySelector( '[data-dialog]' ),
            somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
            dlg = new DialogFx( somedialog );

          dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );

        })();
      </script>
      <script type="text/javascript">
        // $('#security_order').validate({
        //     rules: {
        //         name: {
        //             required: true,
        //         },
        //         email: {
        //             required: true,
        //             email:true,
        //         },
        //         mobile_no: {
        //             required: true,
        //         },
        //     },
        //     messages: {
        //         name: {
        //             required: "Please enter name",
        //         },
        //         email: {
        //           required: "Please enter Email ",
        //           email:"Please enter valid Email",                     
        //         },
        //         mobile_no: {
        //             required: "Please enter mobile_no",
        //         },
        //     }
        // });

        // var form = $( "#security_order" );
        // form.validate();
        // $(document).on('click','.check',function(){
        //   //alert( "Valid: " + form.valid() );
        //   if(form.valid()){
        //     add_security_order();
        //   }
     
        // });
   
       /* function add_security_order(){
           var formData = new FormData($('#security_order')[0]);
            var uurl = BASE_URL+"api/user/add_sucurity_order";

            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response.result=="Success") {
                    $.alert({
                        type: 'green',
                        title: 'Security Order accepted',
                        content: response.message,
                    });
                    setTimeout(function() { window.location.reload(); }, 2000);
                }else if(response.result=="Fail"){
                    $.alert({
                        type: 'red',
                        title: 'Security Order failed',
                        content: response.message,
                    });
                }
                // console.log(response);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }*/
      </script> 
   </body>
</html> 