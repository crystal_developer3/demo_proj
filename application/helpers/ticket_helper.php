<?php

function get_ticket($ticket_id) {
    $CI = &get_instance();
    $where['user_ticket.id'] = $ticket_id;

    $join[0]['table_name'] = 'ticket_location';
    $join[0]['column_name'] = 'ticket_location.id = user_ticket.ticket_location_id';
    $join[0]['type'] = 'left';
    $join[1]['table_name'] = 'user_register';
    $join[1]['column_name'] = 'user_register.id = user_ticket.user_id';
    $join[1]['type'] = 'left';
    $join[2]['table_name'] = 'ticket_status';
    $join[2]['column_name'] = 'ticket_status.id = user_ticket.ticket_status_id';
    $join[2]['type'] = 'left';
    $join[3]['table_name'] = 'priority';
    $join[3]['column_name'] = 'priority.id = user_ticket.priority_id';
    $join[3]['type'] = 'left';

    // $join[4]['table_name'] = 'department';
    // $join[4]['column_name'] = 'department.id = user_register.department_id';
    // $join[4]['type'] = 'left';

    $ticket_details = $CI->Production_model->jointable_descending(array('user_ticket.*', 'ticket_location.location_name', 'user_register.first_name as name','ticket_status.status_name', 'priority.priority_name'), 'user_ticket', '', $join, 'id', 'desc', $where);
    if (!empty($ticket_details)) {
        return $ticket_details[0];
    } else {
        return array();
    }
}

/**
 * This function is used to get ticket history
 * @param type $ticket_id
 */
function get_ticket_history($ticket_id) {
    $CI = &get_instance();
    $sql = 'SELECT ticket_status.status_name,ticket_activity.* FROM ticket_status,ticket_activity WHERE
            ticket_activity.ticket_status_id = ticket_status.id AND ticket_activity.ticket_id = "' . $ticket_id . '" ORDER BY id ASC';
    $info = $CI->common_model->get_data_with_sql($sql);
    $return = array();
    if ($info['row_count'] > 0) {
        foreach ($info['data'] as $key => &$value) {
            $value['full_name'] = '';
            if ($value['allocated_to_type'] == 'user') {
                $user_name = get_user_info($value['allocated_to_id']);
                if (!empty($user_name)) {
                    $value['full_name'] = isset($user_name['full_name']) ? $user_name['full_name'] : '';
                }
            } else if ($value['allocated_to_type'] == 'admin') {
                $user_name = get_admin_info($value['allocated_to_id']);
                if (!empty($user_name)) {
                    $value['full_name'] = isset($user_name['full_name']) ? $user_name['full_name'] : '';
                }
            }
            $value['allocated_by_full_name'] = '';
            if ($value['allocated_by_type'] == 'user') {
                $user_name = get_user_info($value['allocated_by_id']);
                if (!empty($user_name)) {
                    $value['allocated_by_full_name'] = isset($user_name['full_name']) ? $user_name['full_name'] : '';
                }
            } else if ($value['allocated_by_type'] == 'admin') {
                $user_name = get_admin_info($value['allocated_by_id']);
                if (!empty($user_name)) {
                    $value['allocated_by_full_name'] = isset($user_name['full_name']) ? $user_name['full_name'] : '';
                }
            }
            //$value['full_name'] = $value['allocated_by_full_name'];
        }
        $return = $info['data'];
    }
    return $return;
}

function get_timer_start_time($ticket_id) {
    $CI = &get_instance();
    $sql = 'SELECT allocated_time,used_time,ticket_status_id FROM ticket_activity WHERE ticket_id = "' . $ticket_id . '" AND ticket_status_id IN (7,5) ORDER BY id DESC LIMIT 1';
    $info = $CI->common_model->get_data_with_sql($sql);
    $return = '';
    if ($info['row_count'] > 0) {
        $current_time = $info['data'][0]['allocated_time'];
        $used_time = $info['data'][0]['used_time'];
        $used_time = explode(':', $used_time);
        $sub_string = '-' . $used_time[0] . ' days -' . $used_time[1] . 'hours -' . $used_time[2] . ' minutes -' . $used_time[3] . ' seconds';
        $current_time = date('Y-m-d H:i:s', strtotime($sub_string, strtotime($current_time)));
        $return = $current_time;
    }
    return $return;
}

function ticket_status_admin() {
    return array('5' => 'In Progress', '4' => 'Hold', '9' => 'Closed', '7' => 'Queue');
}

function get_priority($priority_id = '') {
    $CI = &get_instance();
    if ($priority_id != '') {
        $conditions = array('where' => array('id' => $priority_id));
    } else {
        $conditions = array();
    }
    $info = $CI->common_model->select_data('priority', $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}

function ticket_inprogress_available($ticket_id) {
    $sql = 'SELECT ticket_id FROM ticket_activity WHERE 
           ticket_status_id = "5" AND ticket_id = "' . $ticket_id . '" LIMIT 1 ';
    $CI = &get_instance();
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        return true;
    } else {
        return false;
    }
}

function send_ticket_closed_email($ticket_id) {
    $CI = &get_instance();
    $CI->load->model('email_model');
    $info = get_ticket($ticket_id);
    if (!empty($info)) {
        /* For sending an email to department */
        $user_info = get_user_info($info['user_id']);
        if (!empty($user_info)) {
            $department_info = get_department($user_info['department_id']);
            $records = array_merge($info,$user_info);            
            if (!empty($department_info)) {      
                $location = get_location($records['location_id']);
                $image_path = '';
                if (!empty($location)) {
                    $image_path = base_url() . 'uploads/location/' . $location[0]['location_logo'];
                }
                $department_info = $department_info[0];
                $department_info = json_decode($department_info['email_address'], true);
                foreach ($department_info as $key => $value) {
                    $html = '';
                    $html .= $CI->email_model->get_email_header($image_path);
                    ob_start();
                    ?>
                    <div class="discription">
                        <h2 style="margin:0;text-align:center;">Ticket Closed</h2>
                    </div>
                    <table>
                        <tr>
                            <td colspan="2">
                                <p>Your ticket has been closed</p>
                                <p>Please find details below</p>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Ticket Number:</strong></td>
                            <td><?php echo $records['ticket_no']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Created At:</strong></td>
                            <td><?php echo date('d-m-Y h:i A', strtotime($records['create_date'])); ?></td>
                        </tr>                   
                        <tr>
                            <td><strong>Location:</strong></td>
                            <td>
                                <?php
                                echo isset($location[0]['location_name']) ? $location[0]['location_name'] : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Department name:</strong></td>
                            <td>
                                <?php
                                echo isset($records['department_name']) ? $records['department_name'] : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Priority:</strong></td>
                            <td>
                                <?php
                                echo isset($records['priority_name']) ? $records['priority_name'] : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>User:</strong></td>
                            <td>
                                <?php
                                echo isset($records['full_name']) ? $records['full_name'] : '';
                                ?>
                            </td>
                        </tr>
                        
                        <?php                        
                        if (isset($records['problem']) && trim(strip_tags($records['problem'])) != '') {
                            ?>
                            <tr>
                                <td><strong>Problem:</strong></td>
                                <td>
                                    <div class="content">
                                        <?php echo closetags($records['problem']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <?php
                    $html .= ob_get_clean();
                    $html .= $CI->email_model->get_email_footer();
                    $settings = array(
                        'to' => array($value),
                        'content' => $html,
                        'subject' => 'Ticket Closed'
                    );
                    send_email($settings);
                }
            }
        }
    }
}

function get_ticket_from_number($ticket_no) {
    $CI = &get_instance();
    $sql = 'SELECT id FROM user_ticket WHERE ticket_no = "' . $ticket_no . '"';
    $info = $CI->common_model->get_data_with_sql($sql);
    if ($info['row_count'] > 0) {
        $ticket_id = $info['data'][0]['id'];
        $sql = 'SELECT ticket_id,allocated_to_type,allocated_to_id FROM ticket_activity WHERE
            ticket_id = "' . $ticket_id . '" ORDER BY id DESC LIMIT 1 ';
        $info = $CI->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            $info = $info['data'][0];
            $info = array_merge($info, get_ticket($info['ticket_id']));
            if ($info['allocated_to_type'] == 'admin') {
                $user_info = get_admin_info($info['allocated_to_id']);
                if (!empty($user_info)) {
                    $info['allocated_to'] = $user_info['full_name'];
                }
            } else if ($info['allocated_to_type'] == 'user') {
                $user_info = get_user_info($info['allocated_to_id']);
                if (!empty($user_info)) {
                    $info['allocated_to'] = $user_info['name'];
                }
            }
            return $info;
        } else {
            return array();
        }
    } else {
        return array();
    }
}


if (!function_exists('get_ticket_notification')) {
    function get_ticket_notification() {
        $CI = & get_instance();
        $conditions = array("select" => "*", "where" => array("notification" => "0"));
        $info = $CI->common_model->select_data("tickets", $conditions);
        if ($info['row_count'] > 0) {
            return $info['row_count'];
        } else {
            return 0;
        }
    }
}