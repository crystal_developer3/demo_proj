<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Social_links extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {
        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/social-links/view/'),'social_links','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/social-links/view',$data);
    }

    function add()
    {
        $data['social_links'] = array();
        $this->load->view('authority/social-links/add-edit',$data);
    }

    function add_links()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d h:i:s');
        //echo"<pre>"; print_r($data); exit;
        
        $record = $this->Production_model->insert_record('social_links',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Social link Add Successfully....!');
            redirect(base_url('authority/social-links/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Social link Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['social_links'] = $this->Production_model->get_all_with_where('social_links','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/social-links/add-edit',$data);
    }

    function update_links()
    {
        $about_id = $this->input->post('id');
        $data = $this->input->post();
        $data['modified_date'] = date('Y-m-d H:i:s');
        // echo"<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('social_links',$data,array('id'=>$about_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Social link Update Successfully....!');
            redirect(base_url('authority/social-links/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Social link Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        
        $record = $this->Production_model->delete_record('social_links',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Social link Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Social link Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
?>