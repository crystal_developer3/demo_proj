<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view() {
        // $settings = array(
        //     "url" => site_url() . "authority/userlist/view/",
        //     "per_page" => RECORDS_PER_PAGE,
        // );
  //       $current_filter_users = array('search_key'=> '', 'search_value' => '');
		// if(isset($_REQUEST['clear_filter'])){
		// 	$this->session->current_filter_users = $current_filter_users;
		// 	redirect(base_url().'authority/userlist/view');
		// }
		
		// if(!empty($this->session->current_filter_users)){
		// 	$current_filter_users = $this->session->current_filter_users;
		// }
		// if ($this->input->post("submit")) {
		// 	if($this->input->post('search_key') != '') {
		// 		$current_filter_users['search_key'] = $this->input->post('search_key');
		// 	}
		// 	if($this->input->post('search_value') != '') {
		// 		$current_filter_users['search_value']= $this->input->post('search_value');
		// 	}
		// }
		
		// $like = array();
		// if($current_filter_users['search_key'] != '' && $current_filter_users['search_value']){
		// 	$like = array('LIKE'=>array($current_filter_users['search_key']=>$current_filter_users['search_value']));
		// }
		// if(!empty($like)){
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// 	$conditions = array_merge($conditions,$like);
		// } else {
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// }
  //       $data = $this->common_model->get_pagination("user_register", $conditions, $settings);
  //       // echo"<pre>"; print_r($data); exit;

  //       if (isset($this->session->users_msg) && $this->session->users_msg != '') {
  //           $data = array_merge($data, array("success" => $this->session->users_msg));
  //           $this->session->users_msg = '';
  //       }
  //       unset($settings, $conditions);

  //       $this->session->current_filter_users = $current_filter_users;
		// $data['current_filter_users'] = $current_filter_users;
		// $data['search_options'] = $this->_search_array;
  //       $conditions = array("select" => "*");

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/contact_us/view/'),'contact_us','','id','desc',array());
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;

        // ==================== pagination end ======================== //
        $this->load->view('authority/contact_us/view', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id", "where" => array("id" => intval($id)));

            $user = $this->common_model->select_data("contact_us", $conditions);
            // echo"<pre>"; echo $this->db->last_query(); print_r($user); exit;
           
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data("contact_us", $conditions);
            // $this->session->user_msg = "Record deleted successfully";
            $this->session->set_flashdata('success','Record deleted successfully!!!');
            redirect("authority/contact_us/view");
        } else {
            redirect("authority/contact_us/view");
        }
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("contact_us", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/contact_us/view");
            }
        } else {
            redirect("authority/contact_us/view");
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/contact_us/details', $data);
    } 

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {

            $get_record = $this->Production_model->get_all_with_where('contact_us','','',array('id'=>$value));
            // if ($get_record !=null && $value['category_image'] !=null && !empty($value['category_image']))   
            // {
            //     unlink(CATEGORY_IMAGE.$value['category_image']);
            // }
            $record = $this->Production_model->delete_record('contact_us',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Record deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Record Not deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }    
}
?>