<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
    private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function view() {
        $data = array();       
        // ==================== pagination start ======================== //
        $total_records = $this->Production_model->get_total('feedback');
        if ($total_records !=null) 
        {    
            // ==================== pagination start ======================== //
            $tmp_data = $this->Production_model->get_all_with_where('feedback','','',array());
            $tmp_array['total_record'] = count($tmp_data);
            $tmp_array['url'] = base_url('authority/feedback/view/');
            $tmp_array['per_page'] = RECORDS_PER_PAGE;
            $record = $this->Production_model->only_pagination($tmp_array);
            $data['user_details'] = $this->Production_model->get_all_with_where_limit('feedback','id','desc',array(),$record['limit'],$record['start']); 
            $data['pagination'] = $record['pagination']; 
            $data['no'] = $record['no']; 
            // echo"<pre>"; print_r($data); exit;
            // ==================== pagination end ======================== //
            //====================== user filter start ======================//
            $start_date=$this->input->post('start_date');
            $end_date=$this->input->post('end_date');
            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where_limit('feedback','id','asc',array(),$record['limit'],$record['start']); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('feedback','id','asc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$record['limit'],$record['start']); 
                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
            //====================== user filter end ======================//
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        // ==================== pagination end ======================== //
        $this->load->view('authority/feedback/view', $data);
    }
    public function edit($id = "") {
        $this->load->helper("form");
        $data = array("form_title" => "Edit Profile");
        if ($id != "") {
            $conditions = array("where" => array("user_id" => $id));
            $user_info = $this->common_model->select_data("feedback", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/feedback/view");
            }
        } else {
            redirect("authority/feedback/view");
        }
        /* Form Validation */
        $this->form_validation->set_rules('full_name', 'full name', 'required', array('required' => 'Please enter user name'));
        $this->form_validation->set_rules('email_address', 'email addres', 'required|valid_email|is_unique_with_except_record[user.email_address.id.' . $data['user_id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        $this->form_validation->set_rules('mobile_number_1', 'mobile number', 'required|numeric|min_length[10]|max_length[10]', array('required' => 'Please enter mobile number'));
        $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'numeric|min_length[10]|max_length[10]');
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;
            if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
                $config['upload_path'] = './uploads/profile_photo/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profile_photo')) {
                    $error_msg = array('error' => $this->upload->display_errors());
                    $error = true;
                } else {
                    /* delete old photo */
                    if (!is_null($data['profile_photo'])) {
                        $path = $config['upload_path'] . $data['profile_photo'];
                        if (is_file($path)) {
                            @unlink($path);
                        }
                    }
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name = $upload_data['file_name'];
                    chmod($config['upload_path'] . $file_name, 0777);
                    $records["profile_photo"] = $file_name;
                }
            } else {
                $records["profile_photo"] = $data['profile_photo'];
            }
            if (!$error) {
                $conditions = array(
                    "where" => array("id" => $data['id']),
                );
                $this->common_model->update_data("user", $records, $conditions);
                if ($data['id'] == $this->session->user_info['id']) {
                    $this->common_functions->updatesession($data['id']);
                }
                $data = array_merge($data, $_POST);
                $data = array_merge($data, array("success" => "Record updated successfully"));
                $_POST = array();
                $data = array_merge($data, $_POST);
            }
        }
        $this->load->view('authority/user/add-edit', $data);
    }
    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data("feedback", $conditions);            
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data("feedback", $conditions);
            $this->session->user_msg = "Record deleted successfully";
            redirect("authority/feedback/view");
        } else {
            redirect("authority/feedback/view");
        }
    }
    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("feedback", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/feedback/view");
            }
        } else {
            redirect("authority/feedback/view");
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/feedback/details', $data);
    }
    function excel_genrate_report()
    {       
        $tmp = array();
        // ==================== pagination start ======================== //
        $total_records = $this->Production_model->get_total('feedback');
        if ($total_records !=null) 
        {    
            $config = array();
            $config["base_url"] = base_url() . 'authority/feedback/view/';
            $config["total_rows"] = $total_records;
            $config["per_page"] = RECORDS_PER_PAGE;
            $config['use_page_numbers'] = TRUE;
            $config["uri_segment"] = 4;
            $config['num_links'] = $total_records;
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Previous'; 
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li class="end">';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            if ($page_number > ceil(($config['total_rows'] / $config['per_page']))) {
                /* Redirect when page limit exceeded */
                redirect($config['base_url']);
            }
            /* GETTING A OFFSET */
            $page = ($page_number == 0) ? 0 : ($page_number * $config['per_page']) - $config['per_page'];
            //echo"<pre>"; print_r($page); exit;
            //====================== user filter start ======================//
            $start_date=$this->input->post('start_date');
            $end_date=$this->input->post('end_date');
            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where_limit('feedback','id','asc',array(),$config["per_page"], $page); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('feedback','id','asc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$config["per_page"], $page); 
                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
            //====================== user filter end ======================//
            // $data["user_details"] = $this->Production_model->get_all_with_where_limit('feedback','user_id','asc',array(),$config["per_page"], $page); // list for all users ....
            $data['links'] = $this->pagination->create_links();  
            $data['no'] = $page+1;
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        // ==================== pagination end ======================== //
        $tmp = array_merge($tmp,$data['user_details']);
        // excel export data strart //
        $excel_data = array();
        foreach ($tmp as $key => $value) {
            $tmp = array(
                'First Name' => $value['first_name'],
                'Last Name' => $value['last_name'],
                'User Email' => $value['user_email'],
                'Mobile No' => $value['mobile_no'],
                'Create Date' => date('d-m-Y',strtotime($value['create_date'])),
            );          
            array_push($excel_data,$tmp);
        }
        // echo"<pre>"; print_r($excel_data); exit;
        function filterData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        // file name for download
        $fileName = "Feedback-report" . date('d-m-Y') . ".xls";
        // headers for download
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        foreach($excel_data as $row) {
            if(!$flag) {
                // display column names as first row
                echo implode("\t", array_keys($row)) . "\n";
                $flag = true;
            }
            // filter data
            array_walk($row, 'filterData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
        // excel export data end //
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_record = $this->Production_model->get_all_with_where('feedback','','',array('id'=>$value));
            $record = $this->Production_model->delete_record('feedback',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Record deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Record Not deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>