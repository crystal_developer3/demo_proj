<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Profile extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();	
			if (!$this->session->userdata('login_id')) {
				redirect(base_url('login'));
			}		
		}

		function index()
		{
			$data = array();
			// $data['wishlist_details'] = array();
			/*End*/
			// echo"<pre>";echo $this->db->last_query(); print_r($data['wishlist_details']); exit;
			
			$this->load->view('profile',$data);	
		}

		function update_profile(){
			$data = $this->input->post();
			$data['mobile_no_display'] = $this->input->post('mobile_no_display') ==null ? '1' : '0';
			// echo"<pre>"; print_r($data); exit;

			/* Form Validation */
	        $this->form_validation->set_rules('name', 'Name', 'required', array('required' => 'Please enter user name'));        
	        $this->form_validation->set_rules('user_email', 'email address', 'required|valid_email|is_unique_with_except_record[user_register.user_email.user_id.' . $this->session->userdata('login_id') . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));       
	        $this->form_validation->set_rules('mobile_no', 'mobile number', 'required|numeric|min_length[10]|max_length[10]', array('required' => 'Please enter mobile number'));

	        $this->form_validation->set_rules('mobile_no', 'Mobile No', 'required|is_unique_with_except_record[user_register.mobile_no.user_id.' . $this->session->userdata('login_id') . ']', array('required' => 'Please enter Mobile No', "is_unique_with_except_record" => "This Mobile No is already available"));    

	        $this->form_validation->set_rules('id_country', 'Country', 'required', array('required' => 'Please select Country'));
	        $this->form_validation->set_rules('id_state', 'State', 'required', array('required' => 'Please select State'));        
	        $this->form_validation->set_rules('id_city', 'City', 'required', array('required' => 'Please select City')); 

	        if ($this->form_validation->run() === FALSE) {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);
	        } else {

	        	$record = $this->Production_model->update_record('user_register',$data,array('user_id'=>$this->session->userdata('login_id')));
		        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
		        if ($record == 1) {
		            $this->session->set_flashdata('success', 'Profile Update Successfully...!');
		            redirect($_SERVER['HTTP_REFERER']);
		        }
		        else
		        {
		            $this->session->set_flashdata('error', 'Profile Not Updated...!');
		            redirect($_SERVER['HTTP_REFERER']);
		        }
	        }
		}

		public function changepassword() {
	        // $data = array("form_title" => "Change Password");
	        $this->form_validation->set_rules('current_password', 'current password', 'required|min_length[6]', array('required' => 'Please enter current password'));
	        $this->form_validation->set_rules('new_password', 'new password', 'required|min_length[6]', array('required' => 'Please enter new password'));
	        $this->form_validation->set_rules('confirm_new_password', 'confirm new password', 'required|min_length[6]|matches[new_password]', array('required' => 'Please enter confirm password', "matches" => "Password and confirm password should be same"));

	        if ($this->form_validation->run() === FALSE) {
	            $data = array_merge($data, $_POST);
	        } else {
	            $current_password = $this->input->post("current_password");
	            $get_record = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$this->session->userdata('login_id')));
				$check = $this->encryption->decrypt($get_record[0]['password']); 

				if($check != $current_password){           
	                // $data = array_merge($data, array("error" => "Please enter correct old password","current_password_error" => "Please enter correct old password"));
	                $this->session->set_flashdata('error', 'Please enter correct old password...!');
		            redirect($_SERVER['HTTP_REFERER']);
	                // echo"<pre>"; print_r($data); exit;
	            } else {
	                $new_password = $this->input->post("new_password");
	                $records = array(
	                    "password" => $this->encryption->encrypt($new_password),
	                );
	                $conditions = array(
	                    "where" => array("user_id" => $this->session->userdata('login_id')),
	                );
	                $this->common_model->update_data('user_register', $records, $conditions);
	                $this->session->set_flashdata('success', 'Password changed successfully...!');
		            redirect($_SERVER['HTTP_REFERER']);
	                // $_POST = array();
	                // $data = array_merge($data, array("success" => "Password changed successfully"));
	            }
	        }
	        redirect($_SERVER['HTTP_REFERER']);
	    }

	    public function upload_profile_photo() {
	        $response_array = array('success' => false);
	        $error = false;
	        $error_messages = array();
	        if (isset($_FILES['profile_picture']) && $_FILES['profile_picture']['name'] != "") {
	        	// echo"<pre>"; print_r($_FILES); exit;

	            if ($_FILES["profile_picture"]["size"] >= MAX_FILE_SIZE_IMAGE) {
	                $response_array['message'] = 'Please upload file with size less than ' . (MAX_FILE_SIZE_IMAGE / 1000000) . 'MB';
	                $error = true;
	            }
	            if (!$error) {
	                $config['upload_path'] = './assets/uploads/profile_picture/';                
	                if (!is_dir($config['upload_path'])) {
	                    mkdir($config['upload_path']);
	                    @chmod($config['upload_path'], 0777);

	                    mkdir($config['upload_path'] . 'thumb/');
	                    @chmod($config['upload_path'] . 'thumb/', 0777);    
	                }
	                $config['allowed_types'] = 'gif|jpg|png|jpeg';
	                //$config['max_size'] = MAX_FILE_SIZE_IMAGE;
	                $config['encrypt_name'] = TRUE;
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if (!$this->upload->do_upload('profile_picture')) {
	                    $response_array['message'] = $this->upload->display_errors();
	                    $error = true;
	                } else {

	                    /* delete old photo */
	                    $get_image = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$this->session->userdata('login_id')));

	                    /*Old image delete*/
		                if ($get_image !=null && $get_image[0]['profile_picture'] !=null && !empty($get_image[0]['profile_picture']))
		                {
		                    @unlink(PROFILE_PICTURE.$get_image[0]['profile_picture']);
		                }
	                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

	                    // echo"<pre>"; print_r($upload_data); exit;

	                    $file_name = $upload_data['file_name'];
	                    // $this->generate_thumbnail($file_name);
	                    @chmod($config['upload_path'] . $file_name, 0777);

	                    $records = array();
	                    $records["profile_picture"] = $file_name;
	                    $conditions = array(
	                        'where' => array('user_id' => $this->session->userdata('login_id')),
	                    );
	                    $this->common_model->update_data('user_register', $records, $conditions);
	                    // $this->common_functions->update_candidate_session($this->session->front_user_data['id']);
	                    $session = array(
							'profile_picture' => $file_name
						);		
						$this->session->set_userdata($session);

	                    $response_array['success'] = true;
	                    $response_array['message'] = 'Profile Photo updated successfully';
	                }
	            }
	        } else {
	            $response_array['message'] = 'Please select image';
	        }
	        echo json_encode($response_array);
	        exit;
	    } 
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>