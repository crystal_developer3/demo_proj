<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
*************************************************************************************************************
*
* If You Want To Working With Import And Export Data In Excel You Need To Download PHPExcel Plugin.
* Here Is Github Link To Download 'https://github.com/PHPOffice/PHPExcel'
*
* After Downloading , Copy That File In Project Folder and include in this Library
*
*************************** Export Data As Excel ************************************************************
*
* You Need To Define Title By $data['Title'] = 'title of excel shet';
* Name ::  $data['FileName'] = "City-List-".date("Y-m-d");//Give the Dynamic name to the excel file
* Define Header List ::  $data['HeaderList'] = array("","","","","");
* Data :: Query to database $data['ExcelData'] = 'result from database ';
* Call The Function From Controller ::  $this->libraryname->export_excel($data);
*
*************************** Import Data From Excel Sheet ****************************************************
*
* Pass The File Name :: $data['FileName'] = $FileName;
* Set Header :: $data['FieldsList'] = array("","","","","","");
* Call The Function From Controller :: $excel_data = $this->libraryname->import_excel($path,$data);
*************************************************************************************************************/
require_once APPPATH.'third_party/excel/PHPExcel.php';
class Excel extends PHPExcel {
	public $CI;
    public function __construct() { 
        parent::__construct(); 
        $this ->CI=&get_instance();
    } 
    public function export_excel($data)
    {
        $this->CI->excel->setActiveSheetIndex(0);
        $this->CI->excel->getActiveSheet() -> setTitle($data['Title']);
        $column="A";
        foreach ($data['HeaderList'] as $row)
        {
            $this ->CI->excel->getActiveSheet()->setCellValue($column."1",$row);
            $this ->CI->excel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            $this ->CI->excel->getActiveSheet()->getStyle($column."1")->getFont()->setBold(true);
            $this ->CI->excel->getActiveSheet()->getStyle($column."1")->getFont()->setSize(12);
            $column++;
        }
        //select data from inventory table
        $sql=$data['ExcelData'];
        $exceldata="";
        foreach ($sql as $row){
            $exceldata[] = $row;
        }
        //Fill data
        $this ->CI->excel->getActiveSheet()->fromArray($data['ExcelData'], null, 'A2');
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$data['FileName'].'.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this ->CI->excel , 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function import_excel($path,$data)
    {
        //$file = 'assets/uploads/excel/'.$data['FileName'];
        $file = $path.$data['FileName'];
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        $i = 0;
        $arr_data = array();
        foreach ($cell_collection as $cell)
        {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            if ($row > 1)
            {
                $arr_data[$row - 2][$data['FieldsList'][$i++]] = $data_value;
            }
            if($i == count($data['FieldsList'])){
                $i = 0;
            }
        }
        return $arr_data;
    }
    function upload_File($field_name, $upload_path, $file_name, $allowed_types)
    {
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, TRUE);
             @chmod($upload_path,0777);
        }
        $config['file_name'] = $file_name;
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;
        $config['overwrite'] = true;
        $config['remove_spaces'] = TRUE;
        $CI->upload->initialize($config);

        $FileName = "";
        if ($this ->CI->upload->do_upload($field_name)) {
            $file = $this ->CI->upload->data();
            $FileName = $file['file_name'];
            chmod($file['full_path'],0777);
        } else {
            $error = $this ->CI->upload->display_errors();
            return $error;
        }
        return $FileName;
    }
}
